package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public interface Observable {
	
	public void attach(Observer observer);
	
	public void detach(Observer observer);
	
	public void notifyObs() throws SensorNotActivatedException;

}
