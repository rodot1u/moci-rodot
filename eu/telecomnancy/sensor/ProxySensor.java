package eu.telecomnancy.sensor;

import java.util.ArrayList;
import java.util.Date;

import eu.telecomnancy.ui.Observer;

public class ProxySensor implements ISensor {

	private ArrayList<String> log = new ArrayList<String>();
	private ArrayList<Observer> listObs = new ArrayList<Observer>();
    private State state;
    private boolean status = false;
    double value = 0;
    

    @Override
    public void on() {
        state = new StateOn();
        status = true;
        
        Date d=new Date();
        String day = d.getDay()+"/"+d.getMonth()+"/"+d.getYear();
        log.add("Date : " +day+"\nFonction appelée : on()\nPas de valeur de retour" );
        System.out.println(log.get(log.size() - 1));
    }

    @Override
    public void off() {
        state = new StateOff();
        status = false;
        
        Date d=new Date();
        String day = d.getDay()+"/"+d.getMonth()+"/"+d.getYear();
        log.add("Date : " +day+"\nFonction appelée : off()\nPas de valeur de retour" );
        System.out.println(log.get(log.size() - 1));
    }

    @Override
    public boolean getStatus() {
    	Date d=new Date();
        String day = d.getDay()+"/"+d.getMonth()+"/"+d.getYear();
        log.add("Date : " +day+"\nFonction appelée : getStatus()\nValeur de retour : " +status );
        System.out.println(log.get(log.size() - 1));
        return status;
    }

    @Override
    public void update() throws SensorNotActivatedException {
    	Date d=new Date();
        String day = d.getDay()+"/"+d.getMonth()+"/"+d.getYear();
        log.add("Date : " +day+"\nFonction appelée : update()\nPas de valeur de retour" );
        System.out.println(log.get(log.size() - 1));
        state.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
    	Date d=new Date();
        String day = d.getDay()+"/"+d.getMonth()+"/"+d.getYear();
        log.add("Date : " +day+"\nFonction appelée : getValue()\nValeur de retour : " +state.getValue() );
        System.out.println(log.get(log.size() - 1));
        return state.getValue();
    }

	@Override
	public void attach(Observer observer) {
		if(observer != null){
			listObs.add(observer);
		}
		Date d=new Date();
        String day = d.getDay()+"/"+d.getMonth()+"/"+d.getYear();
        log.add("Date : " +day+"\nFonction appelée : attach()\nPas de valeur de retour" );
        System.out.println(log.get(log.size() - 1));
		
	}

	@Override
	public void detach(Observer observer) {
		if(listObs != null){
			listObs.remove(observer);
		}
		Date d=new Date();
        String day = d.getDay()+"/"+d.getMonth()+"/"+d.getYear();
        log.add("Date : " +day+"\nFonction appelée : detach()\nPas de valeur de retour" );
        System.out.println(log.get(log.size() - 1));
		
	}

	@Override
	public void notifyObs() throws SensorNotActivatedException {
		for (Observer observer : listObs){
			observer.update();
		}
		Date d=new Date();
        String day = d.getDay()+"/"+d.getMonth()+"/"+d.getYear();
        log.add("Date : " +day+"\nFonction appelée : notifyObs()\nPas de valeur de retour" );
        System.out.println(log.get(log.size() - 1));
		
	}

}
