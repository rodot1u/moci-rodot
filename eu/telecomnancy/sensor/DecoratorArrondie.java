package eu.telecomnancy.sensor;

import java.util.ArrayList;

import eu.telecomnancy.ui.Observer;

public class DecoratorArrondie implements ISensor {

	private ArrayList<Observer> listObs = new ArrayList<Observer>();
    private State state;
    private boolean status = false;
    double value = 0;

    @Override
    public void on() {
        state = new StateOn();
        status = true;
    }

    @Override
    public void off() {
        state = new StateOff();
        status = false;
    }

    @Override
    public boolean getStatus() {
        return status;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        state.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        return Math.round(state.getValue());
    }

	@Override
	public void attach(Observer observer) {
		if(observer != null){
			listObs.add(observer);
		}
		
	}

	@Override
	public void detach(Observer observer) {
		if(listObs != null){
			listObs.remove(observer);
		}
		
	}

	@Override
	public void notifyObs() throws SensorNotActivatedException {
		for (Observer observer : listObs){
			observer.update();
		}
		
	}

}
