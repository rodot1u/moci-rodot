package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.Adapter;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class Applts {

    public static void main(String[] args) {
    	LegacyTemperatureSensor lts = new LegacyTemperatureSensor();
    	ISensor sensor = new Adapter(lts);
    	new ConsoleUI(sensor);
    }
}

