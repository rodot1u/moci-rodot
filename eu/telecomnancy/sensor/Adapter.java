package eu.telecomnancy.sensor;

import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.ui.Observer;

public class Adapter extends LegacyTemperatureSensor implements ISensor{
	
	private LegacyTemperatureSensor lts;

	public Adapter(LegacyTemperatureSensor lts){
		this.lts=lts;
	}
	
	@Override
	public void on() {
		if(!lts.getStatus()){
			this.lts.onOff();
		}
	}

	@Override
	public void off() {
		if(lts.getStatus()){
			this.lts.onOff();
		}
	}

	@Override
	public boolean getStatus() {
		return this.lts.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if(!lts.getStatus()){
			this.lts.onOff();
		}else{
			this.lts.onOff();
			this.lts.onOff();
		}
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return this.lts.getTemperature();
	}

	@Override
	public void attach(Observer observer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void detach(Observer observer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyObs() {
		// TODO Auto-generated method stub
		
	}
	
}
