package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public interface Observer {
	
	public void update() throws SensorNotActivatedException;
	
}
